const {connection} = require('../config/pgsql')

getDataPg = (message = '', callback) => {
    connection.one(`SELECT * FROM initial WHERE keywords LIKE '%${message}%'  LIMIT 1`)
    .then(results => {
        const key = results?.option_key || null
        callback(key)
    })
    .catch(err => {
        console.log(err)
    });
}


getReplyPg = (option_key = '', callback) => {
    let values = [option_key]
    connection.one('SELECT * FROM response WHERE option_key = $1 LIMIT 1',values)
    .then(results => {
        const value = {
            replyMessage:results?.replymessage || '',
            trigger:results?.trigger || '',
            media:results?.media || ''
        
        }
        callback(value)
    })
    .catch(err => {
        console.log('error >>> 1', err)
    });
}

getMessagesPg = ( number ) => new Promise((resolve,reejct) => { 

        let values = [number]
       
        connection.any('SELECT * FROM response WHERE number = $1',values)
        .then(results => {
            const [response] = results;
            const value = {
                replyMessage:response?.replymessage || '',
                trigger:response?.trigger || '',
                media:response?.media || ''
            }
            resolve(value)
        })
        .catch(err => {
            console.log('error >>> 2', err)
        });
})

saveMessagePgsql = ( message,trigger, number ) => new Promise((resolve,reejct) => {
        let date = new Date()
        let values = [message,date,trigger,number]
        connection.none('INSERT INTO messages(message, date, trigger, number) VALUES ($1,$2,$3,$4)',values)
        .then(results=>{
            resolve(results)
        }).catch(err=>{
            console.log(err)
        })
})

module.exports = {getDataPg, getReplyPg, saveMessagePgsql}