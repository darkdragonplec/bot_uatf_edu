const initOptions = {
    error(error, e) {
        if (e.cn) {
            console.log('CN:', e.cn);
            console.log('EVENT:', error.message || error);
        }
    }
};
    
const pgp = require('pg-promise')(initOptions);
const connection = pgp(`postgresql://${process.env.SQL_USER}:${process.env.SQL_PASS}@${process.env.SQL_HOST}:5432/${process.env.SQL_DATABASE}`);
    
const connect = () => connection.connect()
    .then(obj => {
        // Can check the server version here (pg-promise v10.1.0+):
        const serverVersion = obj.client.serverVersion;
        console.log("Connected to postgres!");
        obj.done(); // success, release the connection;
    })
    .catch(error => {
        console.log('ERROR:', error.message || error);
});


module.exports = {connect,connection}
